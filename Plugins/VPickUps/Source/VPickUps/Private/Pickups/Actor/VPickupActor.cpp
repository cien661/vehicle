// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/Actor/VPickupActor.h"

#include "Components/SphereComponent.h"
#include "Interfaces/VPickupExecutorInterface.h"

// Sets default values
AVPickupActor::AVPickupActor()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SphereComponent;

	SphereComponent->OnComponentBeginOverlap.AddUniqueDynamic(this, &AVPickupActor::OnBeginOverlap);
}

void AVPickupActor::OnBeginOverlap(UPrimitiveComponent* InOtherComponent, AActor* InOtherActor,	UPrimitiveComponent* InSelfComponent, int32 InKey, bool bInSweep, const FHitResult& InHitResult)
{
	if(InOtherActor)
	{
		const auto& Components = InOtherActor->GetComponentsByInterface(UVPickupExecutorInterface::StaticClass());

		for(const auto Component : Components)
		{
			IVPickupExecutorInterface::Execute_ExecutePickup(Component, PickupData);
		}
	}

	SetLifeSpan(.1f);
}
