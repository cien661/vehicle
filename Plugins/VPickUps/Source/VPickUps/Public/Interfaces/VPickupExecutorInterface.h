// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "VPickupExecutorInterface.generated.h"

class UVBasePickup;
// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UVPickupExecutorInterface : public UInterface
{
	GENERATED_BODY()
};

class VPICKUPS_API IVPickupExecutorInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category = "VPickupExecutorInterface")
	void ExecutePickup(const TSet<UVBasePickup*>& InPickupData);
};
