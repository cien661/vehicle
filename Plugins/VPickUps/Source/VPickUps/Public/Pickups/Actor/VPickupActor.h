// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VPickupActor.generated.h"

class UVBasePickup;
class USphereComponent;
UCLASS(ClassGroup="VPickup")
class VPICKUPS_API AVPickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AVPickupActor();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "VPickupActor")
	USphereComponent* SphereComponent{nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VPickupActor")
	TSet<UVBasePickup*> PickupData;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* InOtherComponent, AActor* InOtherActor, UPrimitiveComponent* InSelfComponent, int32 InKey, bool bInSweep, const FHitResult& InHitResult);
};
