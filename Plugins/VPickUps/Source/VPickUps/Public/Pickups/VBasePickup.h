﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VBasePickup.generated.h"


UCLASS(ClassGroup = "VPickup", BlueprintType)
class VPICKUPS_API UVBasePickup : public UDataAsset
{
	GENERATED_BODY()
};
