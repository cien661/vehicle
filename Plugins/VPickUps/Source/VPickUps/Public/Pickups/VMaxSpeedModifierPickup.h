﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VBasePickup.h"
#include "VMaxSpeedModifierPickup.generated.h"


UCLASS(ClassGroup = "VPickup")
class VPICKUPS_API UVMaxSpeedModifierPickup : public UVBasePickup
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "VMaxSpeedModifierPickup")
	FORCEINLINE float GetMaxSpeedModifier() const { return MaxSpeedModifier; }
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "VMaxSpeedModifierPickup")
	float MaxSpeedModifier{250.f};
};
