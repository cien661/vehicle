﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VBasePickup.h"
#include "VPointsPickup.generated.h"


UCLASS(ClassGroup = "VPickup")
class VPICKUPS_API UVPointsPickup : public UVBasePickup
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "VPointsPickup")
	FORCEINLINE int GetPoints() const { return Points; }
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "VPointsPickup")
	int Points{1};
};
