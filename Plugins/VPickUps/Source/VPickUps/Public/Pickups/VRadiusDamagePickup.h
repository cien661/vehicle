﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VBasePickup.h"
#include "VRadiusDamagePickup.generated.h"

UCLASS(ClassGroup = "VPickup")
class VPICKUPS_API UVRadiusDamagePickup : public UVBasePickup
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "VRadiusDamagePickup" )
	FORCEINLINE FRadialDamageParams GetRadialDamageParams() const {return RadialDamageParams; };

	UPROPERTY(BlueprintReadWrite, Category = "VRadiusDamagePickup")
	TWeakObjectPtr<AActor> DamageCauser;
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "VRadiusDamagePickup")
	FRadialDamageParams RadialDamageParams;
};
