﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Controller/VVehicleController.h"

#include "Inputs/VInputComponent.h"

AVVehicleController::AVVehicleController()
{
	VehicleInput = CreateDefaultSubobject<UVInputComponent>(TEXT("VehicleComponent"));
}

void AVVehicleController::SetupInputComponent()
{
	Super::SetupInputComponent();

	VehicleInput->SetInputComponent(InputComponent);
}

void AVVehicleController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	VehicleInput->SetPawn(InPawn);
}
