﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Inputs/VInputComponent.h"

#include "Pawn/VVehiclePawn.h"


UVInputComponent::UVInputComponent()
{
}

void UVInputComponent::SetPawn(APawn* InPawn)
{
	VehiclePawn = Cast<AVVehiclePawn>(InPawn);	
}

void UVInputComponent::SetInputComponent(UInputComponent* InputComponent)
{
	if(ensure(InputComponent) == false) return;

	InputComponent->BindAxis(ForwardAxisName, this, &UVInputComponent::OnForwardAxis);
	InputComponent->BindAxis(RightAxisName, this, &UVInputComponent::OnRightAxis);

	InputComponent->BindAction(ToggleCameraName, EInputEvent::IE_Pressed, this, &UVInputComponent::OnToggleCamera);
	InputComponent->BindAction(StartHandBrakeName, EInputEvent::IE_Pressed, this, &UVInputComponent::OnStartHandBrake);
	InputComponent->BindAction(StopHandBrakeName, EInputEvent::IE_Released, this, &UVInputComponent::OnStopHandBrake);	
}

void UVInputComponent::Deactivate()
{
	Super::Deactivate();

	if(VehiclePawn.IsValid())
	{
		VehiclePawn->MoveForward(0.f);
		VehiclePawn->MoveRight(0.f);
		VehiclePawn->HandbrakePressed();		
	}
}

void UVInputComponent::OnForwardAxis(float InValue)
{
	if(IsActive() && VehiclePawn.IsValid())
	{
		VehiclePawn->MoveForward(InValue);
	}
}

void UVInputComponent::OnRightAxis(float InValue)
{
	if(IsActive() && VehiclePawn.IsValid())
	{
		VehiclePawn->MoveRight(InValue);
	}
}

void UVInputComponent::OnToggleCamera()
{
	if(IsActive() && VehiclePawn.IsValid())
	{
		VehiclePawn->ToggleCamera();
	}
}

void UVInputComponent::OnStartHandBrake()
{
	if(IsActive() && VehiclePawn.IsValid())
	{
		VehiclePawn->HandbrakePressed();
	}	
}

void UVInputComponent::OnStopHandBrake()
{
	if(IsActive() && VehiclePawn.IsValid())
	{
		VehiclePawn->HandbrakeReleased();
	}
}
