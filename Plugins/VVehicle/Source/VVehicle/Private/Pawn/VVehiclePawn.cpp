﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/VVehiclePawn.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

AVVehiclePawn::AVVehiclePawn()
{
	const auto Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(GetVehicleMovement());
	if(ensure(Vehicle4W))
	{
		if(UPrimitiveComponent* UpdatedPrimitive = Cast<UPrimitiveComponent>(Vehicle4W->UpdatedComponent))		
		{
			UpdatedPrimitive->BodyInstance.COMNudge = InertiaTensorScale;
		}
	}

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ChaseCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	
	InternalCameraBase = CreateDefaultSubobject<USceneComponent>(TEXT("InternalCameraBase"));
	InternalCameraBase->SetupAttachment(GetMesh());
	InternalCameraBase->SetRelativeLocation(InternalCameraOrigin);

	InternalCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("InternalCamera"));
	InternalCamera->SetupAttachment(InternalCameraBase);

	InCarSpeed = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarSpeed"));
	InCarSpeed->SetupAttachment(GetMesh());

	InCarGear = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarGear"));
	InCarGear->SetupAttachment(GetMesh());
	
	EngineSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("EngineSound"));
	EngineSoundComponent->SetupAttachment(GetMesh());
}

void AVVehiclePawn::MoveForward(float InValue)
{
	GetVehicleMovementComponent()->SetThrottleInput(InValue);
}

void AVVehiclePawn::MoveRight(float InValue)
{
	GetVehicleMovementComponent()->SetSteeringInput(InValue);
}

void AVVehiclePawn::HandbrakePressed()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AVVehiclePawn::HandbrakeReleased()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AVVehiclePawn::ToggleCamera()
{
	EnableIncarView(!bInCarCameraActive);
}

void AVVehiclePawn::BeginPlay()
{
	Super::BeginPlay();

	InCarSpeed->SetVisibility(bInCarCameraActive);
	InCarGear->SetVisibility(bInCarCameraActive);

	EnableIncarView(bInCarCameraActive);
	EngineSoundComponent->Play();
}

void AVVehiclePawn::Tick(float InDelta)
{
	Super::Tick(InDelta);

	const float RPMToAudioScale = ScaleRPM / GetVehicleMovement()->GetEngineMaxRotationSpeed();
	EngineSoundComponent->SetFloatParameter(EngineAudioRPM, GetVehicleMovement()->GetEngineRotationSpeed() * RPMToAudioScale);
}

void AVVehiclePawn::EnableIncarView(const bool bInState)
{
	if (bInState != bInCarCameraActive)
	{
		bInCarCameraActive = bInState;
		
		if (bInCarCameraActive)
		{
			Camera->Deactivate();
			InternalCamera->Activate();
		}
		else
		{
			InternalCamera->Deactivate();
			Camera->Activate();
		}
		
		InCarSpeed->SetVisibility(bInCarCameraActive);
		InCarGear->SetVisibility(bInCarCameraActive);
	}
}
