﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VVehicleController.generated.h"

class UVInputComponent;
UCLASS(ClassGroup=(VVehicle))
class VVEHICLE_API AVVehicleController : public APlayerController
{
	GENERATED_BODY()

public:
	AVVehicleController();

	FORCEINLINE UVInputComponent* GetInputComponent() const {return VehicleInput;}
protected:
	virtual void SetupInputComponent() override;
	virtual void SetPawn(APawn* InPawn) override;
private:
	UPROPERTY(VisibleAnywhere, Category = "VVehicleController")
	UVInputComponent* VehicleInput{nullptr};
};
