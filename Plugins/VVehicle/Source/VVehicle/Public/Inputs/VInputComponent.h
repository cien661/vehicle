﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VInputComponent.generated.h"

/*
 * Tu by się zdał jakiś pośrednik między pawn'em a componentem
 */
class AVVehiclePawn;
UCLASS(ClassGroup=(VVehicle), meta=(BlueprintSpawnableComponent))
class VVEHICLE_API UVInputComponent : public UActorComponent
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAxisDelegate, const float, InValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FActionDelegate);
	
public:
	UVInputComponent();

	void SetPawn(APawn* InPawn);
	void SetInputComponent(UInputComponent* InputComponent);

	virtual void Deactivate() override;
	
private:
	UFUNCTION()
	void OnForwardAxis(float InValue);
	UFUNCTION()
	void OnRightAxis(float InValue);
	UFUNCTION()
	void OnToggleCamera();
	UFUNCTION()
	void OnStartHandBrake();
	UFUNCTION()
	void OnStopHandBrake();

	UPROPERTY(VisibleInstanceOnly, Category = "VInputComponent")
	TWeakObjectPtr<AVVehiclePawn> VehiclePawn{nullptr};
	
	UPROPERTY(EditDefaultsOnly, Category = "VInputComponent", meta = (AllowPrivateAccess = "true"))
	FName ForwardAxisName{NAME_None};
	UPROPERTY(EditDefaultsOnly, Category = "VInputComponent", meta = (AllowPrivateAccess = "true"))
	FName RightAxisName{NAME_None};
	
	UPROPERTY(EditDefaultsOnly, Category = "VInputComponent", meta = (AllowPrivateAccess = "true"))
	FName ToggleCameraName{NAME_None};
	UPROPERTY(EditDefaultsOnly, Category = "VInputComponent", meta = (AllowPrivateAccess = "true"))
	FName StartHandBrakeName{NAME_None};
	UPROPERTY(EditDefaultsOnly, Category = "VInputComponent", meta = (AllowPrivateAccess = "true"))
	FName StopHandBrakeName{NAME_None};
};

