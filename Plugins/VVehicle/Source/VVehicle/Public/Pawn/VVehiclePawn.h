﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "VVehiclePawn.generated.h"


class UTextRenderComponent;
class USpringArmComponent;
class UCameraComponent;

UCLASS(ClassGroup=(VVehicle))
class VVEHICLE_API AVVehiclePawn : public AWheeledVehicle
{
	GENERATED_BODY()
	
public:
	AVVehiclePawn();

	/** Handle pressing forwards */
	void MoveForward(float InValue);
	/** Handle pressing right */
	void MoveRight(float InValue);
	/** Handle handbrake pressed */
	void HandbrakePressed();
	/** Handle handbrake released */
	void HandbrakeReleased();
	/** Switch between cameras */
	void ToggleCamera();

	FORCEINLINE USpringArmComponent* GetSpringArm() const { return SpringArm; }
	FORCEINLINE UCameraComponent* GetCamera() const { return Camera; }
	FORCEINLINE UCameraComponent* GetInternalCamera() const { return InternalCamera; }
	FORCEINLINE UTextRenderComponent* GetInCarSpeed() const { return InCarSpeed; }
	FORCEINLINE UTextRenderComponent* GetInCarGear() const { return InCarGear; }
	FORCEINLINE UAudioComponent* GetEngineSoundComponent() const { return EngineSoundComponent; }
	
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float InDelta) override;
	
private:
	void EnableIncarView( const bool bInState );
	
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* SpringArm{nullptr};

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera{nullptr};

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InternalCameraBase{nullptr};

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* InternalCamera{nullptr};

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTextRenderComponent* InCarSpeed{nullptr};

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTextRenderComponent* InCarGear{nullptr};

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* EngineSoundComponent{nullptr};

	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bInCarCameraActive{false};

	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bInReverseGear{false};

	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector InternalCameraOrigin{-34.0f, -10.0f, 50.0f};
	
	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector InertiaTensorScale{8.0f, 0.0f, 0.0f};

	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float ScaleRPM{2500.0f};
	UPROPERTY(Category = Camera, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName EngineAudioRPM{"RPM"};
};
