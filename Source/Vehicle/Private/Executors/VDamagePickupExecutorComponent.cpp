// Fill out your copyright notice in the Description page of Project Settings.


#include "Executors/VDamagePickupExecutorComponent.h"
#include "Pickups/VRadiusDamagePickup.h"


UVDamagePickupExecutorComponent::UVDamagePickupExecutorComponent()
{
}

void UVDamagePickupExecutorComponent::ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData)
{
	const auto Owner = GetOwner();
	if(ensure(Owner) == false) return;
	
	for(const auto PickupData : InPickupData)
	{			
		if(const auto RadiusPickup = Cast<UVRadiusDamagePickup>(PickupData))
		{
			FRadialDamageEvent RadialDamageEvent;
			RadialDamageEvent.Origin = RadiusPickup->DamageCauser.IsValid() ? RadiusPickup->DamageCauser->GetActorLocation() : Owner->GetActorLocation();
			RadialDamageEvent.Params = RadiusPickup->GetRadialDamageParams();
			Owner->TakeDamage(RadialDamageEvent.Params.BaseDamage, RadialDamageEvent, nullptr,  RadiusPickup->DamageCauser.Get());
		}
	}
}
