﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Executors/VMaxSpeedModifierExecutorComponent.h"

#include "WheeledVehicleMovementComponent4W.h"
#include "Pickups/VMaxSpeedModifierPickup.h"

UVMaxSpeedModifierExecutorComponent::UVMaxSpeedModifierExecutorComponent()
{
}

void UVMaxSpeedModifierExecutorComponent::Init(UWheeledVehicleMovementComponent4W* InWheeledVehicleMovementComponent4W)
{
	WheeledVehicleMovementComponent4W = InWheeledVehicleMovementComponent4W;
}

void UVMaxSpeedModifierExecutorComponent::ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData)
{
	if(ensure(WheeledVehicleMovementComponent4W) == false) return;
	
	for(const auto PickupData : InPickupData)
	{		
		if(const auto MaxSpeedModifierPickup = Cast<UVMaxSpeedModifierPickup>(PickupData))
		{
			const int LastMaxRPM = WheeledVehicleMovementComponent4W->MaxEngineRPM; 
			WheeledVehicleMovementComponent4W->MaxEngineRPM += MaxSpeedModifierPickup->GetMaxSpeedModifier();

			UpdateMaxRPMDelegate.Broadcast(LastMaxRPM, WheeledVehicleMovementComponent4W->MaxEngineRPM);
		}
	}
}
