﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Executors/VPointsPickupExecutorComponent.h"

#include "GameFramework/PlayerState.h"
#include "Pickups/VPointsPickup.h"


UVPointsPickupExecutorComponent::UVPointsPickupExecutorComponent()
{

}

void UVPointsPickupExecutorComponent::ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData)
{
	const auto Pawn = Cast<APawn>(GetOwner());
	const auto PlayerState = Pawn ? Pawn->GetPlayerState() : nullptr;

	if(ensure(PlayerState) == false) return;

	for(const auto PickupData : InPickupData)
	{
		if(const auto PointsPickup = Cast<UVPointsPickup>(PickupData))
		{
			const auto LastScore = PlayerState->GetScore(); 
			PlayerState->SetScore(LastScore + PointsPickup->GetPoints());

			UpdateScoreDelegate.Broadcast(LastScore, PlayerState->GetScore());
		}
	}
}
