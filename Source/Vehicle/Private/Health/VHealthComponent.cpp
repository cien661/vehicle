﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Health/VHealthComponent.h"


UVHealthComponent::UVHealthComponent()
{
}

void UVHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = StartHealth;
}

float UVHealthComponent::TakeRadialDamage(float InDamage, FRadialDamageEvent const& InRadialDamageEvent, AController* InEventInstigator, AActor* InDamageCauser)
{
	const float LastHealth = CurrentHealth;
	CurrentHealth -= InDamage;
	CurrentHealth = FMath::Clamp(CurrentHealth, 0.f, StartHealth);
	UpdateHealthDelegate.Broadcast(LastHealth, CurrentHealth);

	return InDamage - CurrentHealth;
}

float UVHealthComponent::TakePointDamage(float InDamage, FPointDamageEvent const& InPointDamageEvent, AController* InEventInstigator, AActor* InDamageCauser)
{	
	const float LastHealth = CurrentHealth;
	CurrentHealth -= InDamage;
	CurrentHealth = FMath::Clamp(CurrentHealth, 0.f, StartHealth);
	
	UpdateHealthDelegate.Broadcast(LastHealth, CurrentHealth);

	return InDamage - CurrentHealth;
}
