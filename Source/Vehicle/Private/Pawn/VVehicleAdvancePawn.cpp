﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/VVehicleAdvancePawn.h"

#include "WheeledVehicleMovementComponent4W.h"
#include "Executors/VDamagePickupExecutorComponent.h"
#include "Executors/VMaxSpeedModifierExecutorComponent.h"
#include "Executors/VPointsPickupExecutorComponent.h"
#include "Health/VHealthComponent.h"


AVVehicleAdvancePawn::AVVehicleAdvancePawn()
{
	PointsPickupExecutorComponent = CreateDefaultSubobject<UVPointsPickupExecutorComponent>(TEXT("PointPickupExecutorComponent"));
	DamagePickupExecutorComponent = CreateDefaultSubobject<UVDamagePickupExecutorComponent>(TEXT("DamagePickupExecutorComponent"));
	MaxSpeedModifierExecutorComponent = CreateDefaultSubobject<UVMaxSpeedModifierExecutorComponent>(TEXT("MaxSpeedPickupExecutorComponent"));
	MaxSpeedModifierExecutorComponent->Init(Cast<UWheeledVehicleMovementComponent4W>(GetMovementComponent()));

	HealthComponent = CreateDefaultSubobject<UVHealthComponent>(TEXT("HealthComponent"));
}

void AVVehicleAdvancePawn::Tick(float InDeltaSeconds)
{
	Super::Tick(InDeltaSeconds);

	if(GetActorUpVector().Z < 0.f && HealthComponent->HasAnyHealth())
	{
		TakeDamage(DamageOnCrashPerSec * InDeltaSeconds, FPointDamageEvent(), GetController(), this);
	}
}

float AVVehicleAdvancePawn::InternalTakeRadialDamage(float InDamage, FRadialDamageEvent const& InRadialDamageEvent,	AController* InEventInstigator, AActor* InDamageCauser)
{
	float LeftDamage = Super::InternalTakeRadialDamage(InDamage, InRadialDamageEvent, InEventInstigator, InDamageCauser);

	LeftDamage = HealthComponent->TakeRadialDamage(LeftDamage, InRadialDamageEvent, InEventInstigator, InDamageCauser);

	if(HealthComponent->HasAnyHealth() == false)
	{
		DeadDelegate.Broadcast();
	}

	return LeftDamage;
}

float AVVehicleAdvancePawn::InternalTakePointDamage(float InDamage, FPointDamageEvent const& InPointDamageEvent, AController* InEventInstigator, AActor* InDamageCauser)
{
	float LeftDamage = Super::InternalTakePointDamage(InDamage, InPointDamageEvent, InEventInstigator, InDamageCauser);

	LeftDamage = HealthComponent->TakePointDamage(LeftDamage, InPointDamageEvent, InEventInstigator, InDamageCauser);

	if(HealthComponent->HasAnyHealth() == false)
	{
		DeadDelegate.Broadcast();
	}

	return LeftDamage;	
}
