﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Timer/VStopwatchComponent.h"

UVStopwatchComponent::UVStopwatchComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UVStopwatchComponent::FormatTime(const float& InTime, int& OutMinutes, int& OutSeconds, int& OutMiliseconds)
{
	OutMinutes = GetMinutes(InTime);
	OutSeconds = GetSeconds(InTime);
	OutMiliseconds = GetMiliseconds(InTime);	
}

void UVStopwatchComponent::Start()
{
	bIsRunning = true;
}

void UVStopwatchComponent::Stop()
{
	bIsRunning = false;
}

void UVStopwatchComponent::Reset()
{
	Seconds = 0.f;
}

void UVStopwatchComponent::GetTime(int& OutMinutes, int& OutSeconds, int& OutMiliseconds)
{
	OutMinutes = GetMinutes(Seconds);
	OutSeconds = GetSeconds(Seconds);
	OutMiliseconds = GetMiliseconds(Seconds);
}

int UVStopwatchComponent::GetMinutes(float InTime) 
{
	return InTime / 60.f;
}

int UVStopwatchComponent::GetSeconds(float InTime) 
{
	return InTime - GetMinutes(InTime) * 60.f;
}

int UVStopwatchComponent::GetMiliseconds(float InTime) 
{
	return (InTime - FMath::Floor(InTime)) * 10;
}

void UVStopwatchComponent::TickComponent(float InDeltaTime, ELevelTick InTick, FActorComponentTickFunction* InThisTickFunction)
{
	Super::TickComponent(InDeltaTime, InTick, InThisTickFunction);

	if(bIsRunning)
	{
		Seconds += InDeltaTime;
	}
}
