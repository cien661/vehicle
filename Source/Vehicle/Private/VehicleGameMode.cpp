// Copyright Epic Games, Inc. All Rights Reserved.

#include "Vehicle/Public/VehicleGameMode.h"
#include "Controller/VVehicleController.h"
#include "Executors/VPointsPickupExecutorComponent.h"
#include "GameFramework/PlayerState.h"
#include "Inputs/VInputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Pawn/VVehicleAdvancePawn.h"
#include "Timer/VStopwatchComponent.h"

AVehicleGameMode::AVehicleGameMode()
{
	StopwatchComponent = CreateDefaultSubobject<UVStopwatchComponent>(TEXT("StopwatchComponent"));
}

void AVehicleGameMode::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(StartTimer, this, &AVehicleGameMode::StartRace, StartDelay, false);

	if(const auto VehiclePawn = Cast<AVVehicleAdvancePawn>(UGameplayStatics::GetPlayerPawn(this, 0)))
	{
		VehiclePawn->DeadDelegate.AddUniqueDynamic(this, &AVehicleGameMode::FailRace);

		if(const auto PointsExecutor = VehiclePawn->FindComponentByClass<UVPointsPickupExecutorComponent>())
		{
			PointsExecutor->UpdateScoreDelegate.AddUniqueDynamic(this, &AVehicleGameMode::UpdateScore);
		}
	}
}

void AVehicleGameMode::StartRace() const
{
	StopwatchComponent->Start();

	if(const AVVehicleController* VehicleController = Cast<AVVehicleController>(UGameplayStatics::GetPlayerController(this, 0)))
	{
		VehicleController->GetInputComponent()->Activate();
	}

	StartRaceDelegate.Broadcast();
}

void AVehicleGameMode::FailRace()
{
	HandleEndRace();
	
	FailRaceDelegate.Broadcast();
}

void AVehicleGameMode::WinRace()
{
	HandleEndRace();

	WinRaceDelegate.Broadcast();
}

TArray<FVSaveData> AVehicleGameMode::GetAllResults()
{
	return LoadResults();
}

void AVehicleGameMode::HandleEndRace()
{	
	DeactivateInput();

	DeactivatePawnCollision();
	
	StopwatchComponent->Stop();

	const auto SaveData = CatchDataToSave();	
	auto Scores = LoadResults();

	Scores.Add(SaveData);
	
	const auto SortedResult = SortResults(Scores, ScoreToWin);
	
	SaveResult(SortedResult);
	
	GetWorldTimerManager().SetTimer(RestartTimer, this, &AVehicleGameMode::RestartLevel, EndDelay);
}

void AVehicleGameMode::UpdateScore(const float InLastScore, const float InCurrentScore)
{
	if(InCurrentScore >= ScoreToWin)
	{
		WinRace();
	}
}

void AVehicleGameMode::DeactivateInput() const
{
	if(const AVVehicleController* VehicleController = Cast<AVVehicleController>(UGameplayStatics::GetPlayerController(this, 0)))
	{
		VehicleController->GetInputComponent()->Deactivate();
	}
}

void AVehicleGameMode::DeactivatePawnCollision() const
{
	if(const AVVehicleAdvancePawn* VehiclePawn = Cast<AVVehicleAdvancePawn>(UGameplayStatics::GetPlayerPawn(this, 0)))
	{
		if(USkeletalMeshComponent* SkeletalMeshComponent = VehiclePawn->FindComponentByClass<USkeletalMeshComponent>())
		{
			SkeletalMeshComponent->SetCollisionObjectType(GhostChannel);
		}
	}	
}

void AVehicleGameMode::SaveResult(const TArray<FVSaveData>& InData) const
{
	UVSaveGame* SaveGame = Cast<UVSaveGame>(UGameplayStatics::CreateSaveGameObject(UVSaveGame::StaticClass()));

	for(const auto Data : InData)
	{
		SaveGame->Results.Add(Data);		
	}
	
	UGameplayStatics::SaveGameToSlot(SaveGame, "Save", 0);		
}

TArray<FVSaveData> AVehicleGameMode::SortResults(const TArray<FVSaveData>& InData, const int InMaxTableLong) const
{
	auto SortingData = InData;
	if(SortingData.Num() < 2)
	{
		return SortingData;
	}
	
	Algo::Sort(SortingData, [](const FVSaveData& InA, const FVSaveData& InB)
	{
		return (InA.Score > InB.Score) || (InA.Score == InB.Score && InA.Time < InB.Time);
	});

	while(SortingData.Num() > InMaxTableLong)
	{
		SortingData.RemoveAt(SortingData.Num() -1);
	}

	return SortingData;
}

FVSaveData AVehicleGameMode::CatchDataToSave() const
{
	FVSaveData SaveData;
	if(const auto Pawn = UGameplayStatics::GetPlayerPawn(this, 0))
	{
		SaveData.Score = Pawn->GetPlayerState() ? Pawn->GetPlayerState()->GetScore() : 0.f;
		SaveData.Time = StopwatchComponent->GetSourceTime();
	}

	return SaveData;
}

TArray<FVSaveData> AVehicleGameMode::LoadResults() const
{
	if(const auto SaveGame = Cast<UVSaveGame>(UGameplayStatics::LoadGameFromSlot("Save", 0)))
	{
		return SaveGame->Results;
	}

	return {};
}
