// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/VPickupExecutorInterface.h"
#include "VDamagePickupExecutorComponent.generated.h"


UCLASS(ClassGroup = "Vehicle")
class VEHICLE_API UVDamagePickupExecutorComponent: public UActorComponent, public IVPickupExecutorInterface
{
	GENERATED_BODY()
public:
	UVDamagePickupExecutorComponent();

protected:
	virtual void ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData) override;
};

