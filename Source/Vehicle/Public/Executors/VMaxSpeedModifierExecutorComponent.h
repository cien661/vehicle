﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interfaces/VPickupExecutorInterface.h"
#include "VMaxSpeedModifierExecutorComponent.generated.h"


class UWheeledVehicleMovementComponent4W;
UCLASS(ClassGroup=("Vehicle"), meta=(BlueprintSpawnableComponent))
class VEHICLE_API UVMaxSpeedModifierExecutorComponent : public UActorComponent, public IVPickupExecutorInterface
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUpdateMaxRPMDelegate, const int, InLastMaxRPM, const int, InCurrentMaxRPM);
	
public:
	UVMaxSpeedModifierExecutorComponent();

	void Init(UWheeledVehicleMovementComponent4W* InWheeledVehicleMovementComponent4W);
	
	UPROPERTY(BlueprintAssignable, Category = "VMaxSpeedModifierExecutorComponent")
	FUpdateMaxRPMDelegate UpdateMaxRPMDelegate;
	
protected:
	virtual void ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData) override;

	UPROPERTY(VisibleInstanceOnly, Category = "VMaxSpeedModifierExecutorComponent")
	UWheeledVehicleMovementComponent4W* WheeledVehicleMovementComponent4W{nullptr};
};
