﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interfaces/VPickupExecutorInterface.h"
#include "VPointsPickupExecutorComponent.generated.h"


UCLASS(ClassGroup=("Vehicle"), meta=(BlueprintSpawnableComponent))
class VEHICLE_API UVPointsPickupExecutorComponent : public UActorComponent, public IVPickupExecutorInterface
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUpdateScoreDelegate, const float, InLastScore, const float, InCurrentScore);
public:
	UVPointsPickupExecutorComponent();

	UPROPERTY(BlueprintAssignable, Category = "VPointsPickupExecutorComponent")
	FUpdateScoreDelegate UpdateScoreDelegate;
	
protected:
	virtual void ExecutePickup_Implementation(const TSet<UVBasePickup*>& InPickupData) override; 
};
