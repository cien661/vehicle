﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VHealthComponent.generated.h"

UCLASS(ClassGroup=("Vehicle"), meta=(BlueprintSpawnableComponent))
class VEHICLE_API UVHealthComponent : public UActorComponent
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUpdateHealthDelegate, const float, InLastHealth, const float, InCurrentHealth);
	
public:
	UVHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "VHealthComponent")
	FUpdateHealthDelegate UpdateHealthDelegate;

	UFUNCTION(BlueprintPure, Category = "VHealthComponent")
	FORCEINLINE float GetCurrentHealth() const {return CurrentHealth;};

	FORCEINLINE bool HasAnyHealth() const {return CurrentHealth > 0.f;}
	
	float TakeRadialDamage(float InDamage, FRadialDamageEvent const& InRadialDamageEvent, AController* InEventInstigator, AActor* InDamageCauser);
	float TakePointDamage(float InDamage, FPointDamageEvent const& InPointDamageEvent, AController* InEventInstigator, AActor* InDamageCauser);

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly, Category = "VHealthComponent")
	float StartHealth{3.f};
	
private:
	UPROPERTY(VisibleAnywhere, Category = "VHealthComponent")
	float CurrentHealth{3.f};
};
