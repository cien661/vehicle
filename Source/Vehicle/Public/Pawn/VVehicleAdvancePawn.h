﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/VVehiclePawn.h"
#include "VVehicleAdvancePawn.generated.h"

class UVHealthComponent;
class UVMaxSpeedModifierExecutorComponent;
class UVDamagePickupExecutorComponent;
class UVPointsPickupExecutorComponent;

UCLASS(ClassGroup = "Vehicle")
class VEHICLE_API AVVehicleAdvancePawn : public AVVehiclePawn
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeadDelegate);
	
public:
	AVVehicleAdvancePawn();

	UPROPERTY(BlueprintAssignable, Category = "VVehicleAdvancePawn")
	FDeadDelegate DeadDelegate;

	UFUNCTION(BlueprintPure, Category = "VVehicleAdvancePawn")
	FORCEINLINE UVHealthComponent* GetHealthComponent() const { return HealthComponent; }
	
protected:	
	virtual float InternalTakeRadialDamage(float InDamage, FRadialDamageEvent const& InRadialDamageEvent, AController* InEventInstigator, AActor* InDamageCauser) override;
	virtual float InternalTakePointDamage(float InDamage, FPointDamageEvent const& InPointDamageEvent, AController* InEventInstigator, AActor* InDamageCauser) override;

	virtual void Tick(float InDeltaSeconds) override;
private:
	UPROPERTY(VisibleAnywhere, Category = "VVehicleAdvancePawn")
	float DamageOnCrashPerSec{1.f};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleAdvancePawn")
	UVPointsPickupExecutorComponent* PointsPickupExecutorComponent{nullptr};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleAdvancePawn")
	UVDamagePickupExecutorComponent* DamagePickupExecutorComponent{nullptr};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleAdvancePawn")
	UVMaxSpeedModifierExecutorComponent* MaxSpeedModifierExecutorComponent{nullptr};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleAdvancePawn")
	UVHealthComponent* HealthComponent{nullptr};
};

