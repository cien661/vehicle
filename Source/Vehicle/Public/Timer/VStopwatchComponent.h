﻿#pragma once
#include "VStopwatchComponent.generated.h"

UCLASS(ClassGroup=("Vehicle"), meta=(BlueprintSpawnableComponent))
class VEHICLE_API UVStopwatchComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UVStopwatchComponent();

	UFUNCTION(BlueprintPure, Category = "VStopwatch")
	static void FormatTime(const float& InTime, int& OutMinutes, int& OutSeconds, int& OutMiliseconds);

	UFUNCTION(BlueprintCallable, Category = "VStopwatch")
	void Start();
	UFUNCTION(BlueprintCallable, Category = "VStopwatch")
	void Stop();
	UFUNCTION(BlueprintCallable, Category = "VStopwatch")
	void Reset();
	
	UFUNCTION(BlueprintCallable, Category = "VStopwatch")
	void GetTime(int& OutMinutes, int& OutSeconds, int& OutMiliseconds);
	
	UFUNCTION(BlueprintPure, Category = "VStopwatch")
	static int GetMinutes(float InTime);
	UFUNCTION(BlueprintPure, Category = "VStopwatch")
	static int GetSeconds(float InTime);
	UFUNCTION(BlueprintPure, Category = "VStopwatch")
	static int GetMiliseconds(float InTime);

	FORCEINLINE float GetSourceTime() const { return Seconds; } 
protected:
	virtual void TickComponent(float InDeltaTime, ELevelTick InTickType, FActorComponentTickFunction* InThisTickFunction) override;
	
private:
	UPROPERTY(VisibleAnywhere, Category = "VStopwatch")
	float Seconds{0.f};
	UPROPERTY(VisibleAnywhere, Category = "VStopwatch")
	bool bIsRunning{false};
};