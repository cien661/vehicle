// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/SaveGame.h"
#include "VehicleGameMode.generated.h"

//Sorki za ten bałagan 

USTRUCT(BlueprintType)
struct FVSaveData
{
	GENERATED_BODY()

public:	
	UPROPERTY(BlueprintReadOnly, Category = "VSaveData", SaveGame)
	float Time{0.f};
	UPROPERTY(BlueprintReadOnly, Category = "VSaveData", SaveGame)
	float Score{0.f};
};

UCLASS(ClassGroup = "Vehicle")
class VEHICLE_API UVSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(SaveGame)
	TArray<FVSaveData> Results;
};

class UVStopwatchComponent;
UCLASS(ClassGroup = "Vehicle")
class VEHICLE_API AVehicleGameMode : public AGameModeBase
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUpdateStateRaceDelegate);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateScoreDelegate, const TArray<FVSaveData>&, InScores);
	
public:
	AVehicleGameMode();

	UPROPERTY(BlueprintAssignable, Category = "VVehicleGameMode")
	FUpdateStateRaceDelegate StartRaceDelegate;
	UPROPERTY(BlueprintAssignable, Category = "VVehicleGameMode")
	FUpdateStateRaceDelegate WinRaceDelegate;
	UPROPERTY(BlueprintAssignable, Category = "VVehicleGameMode")
	FUpdateStateRaceDelegate FailRaceDelegate;

	UPROPERTY(BlueprintAssignable, Category = "VVehicleGameMode")
	FUpdateScoreDelegate UpdateScoreDelegate;
	
	UFUNCTION(BlueprintPure, Category = "VVehicleGameMode")
	TArray<FVSaveData> GetAllResults();
	
	UFUNCTION(BlueprintPure, Category = "VVehicleGameMode")
	FORCEINLINE UVStopwatchComponent* GetStopwatchComponent() const { return StopwatchComponent; };
	UFUNCTION(BlueprintCallable, Category = "VVehicleGameMode")
	void GetStartTimer(FTimerHandle& OutTimer) const { OutTimer = StartTimer; };
	UFUNCTION(BlueprintCallable, Category = "VVehicleGameMode")
	void HandleEndRace();
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "VVehicleGameMode")
	void RestartLevel() const;

private:
	UFUNCTION()
	void StartRace()const;
	UFUNCTION()
	void FailRace();
	UFUNCTION()
	void WinRace();

	UFUNCTION()
	void UpdateScore(const float InLastScore, const float InCurrentScore);

	void DeactivateInput() const;
	void DeactivatePawnCollision() const;
	void SaveResult(const TArray<FVSaveData>& InData)const;
	TArray<FVSaveData> SortResults(const TArray<FVSaveData>& InData, const int InMaxTableLong) const;
	
	FVSaveData CatchDataToSave()const;
	TArray<FVSaveData> LoadResults()const;
	
	UPROPERTY(EditDefaultsOnly, Category = "VVehicleGameMode")
	TEnumAsByte<ECollisionChannel> GhostChannel{ECollisionChannel::ECC_Vehicle}; 
	
	UPROPERTY(EditDefaultsOnly, Category = "VVehicleGameMode")
	int ResultToShow{10};
	
	UPROPERTY(EditDefaultsOnly, Category = "VVehicleGameMode")
	float ScoreToWin{10.f};
	
	UPROPERTY(VisibleAnywhere, Category = "VVehicleGameMode")
	float StartDelay{5.f};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleGameMode")
	float EndDelay{5.f};
		
	UPROPERTY(VisibleAnywhere, Category = "VVehicleGameMode")
	UVStopwatchComponent* StopwatchComponent{nullptr};
	UPROPERTY(VisibleAnywhere, Category = "VVehicleGameMode")
	FTimerHandle StartTimer;
	UPROPERTY(VisibleAnywhere, Category = "VVehicleGameMode")
	FTimerHandle RestartTimer;
};



