// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Vehicle : ModuleRules
{
	public Vehicle(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateDependencyModuleNames.AddRange(new string[] { });
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "PhysXVehicles", "HeadMountedDisplay", "PhysicsCore", "VPickUps", "VVehicle" });

		PublicDefinitions.Add("HMD_MODULE_INCLUDED=1");
	}
}
